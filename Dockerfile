# Start with latest golang image
FROM golang:alpine as builder
# Select our working directory

# Install the build tools we need
RUN apk add --no-cache make curl git build-base gcc g++ libc-dev binutils-gold

# Select our working directory
RUN mkdir -p /go/src/github.com/ory/kratos
WORKDIR /go/src/github.com/ory/kratos

# Download the latest version
RUN git clone https://github.com/ory/kratos .

# VERSION is currently based on the last commit
RUN VERSION=$(git describe --tags) \
&& COMMIT=$(git rev-parse HEAD) \
&& BUILD=$(date +%FT%T%z) \
&& LDFLAG_LOCATION="" \
&& LDFLAGS=""

#RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

#RUN GOPROXY=https://goproxy.io go get .
#RUN GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build ${LDFLAGS} -o krakend ./cmd/krakend-ce

# Add BIN Directory
RUN export PATH=".bin:${PATH}"

#DEPS
RUN curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b .bin/ v1.24.0
RUN go build -o .bin/go-acc github.com/ory/go-acc
RUN go build -o .bin/goreturns github.com/sqs/goreturns
RUN go build -o .bin/listx github.com/ory/x/tools/listx
RUN go build -o .bin/mockgen github.com/golang/mock/mockgen
RUN go build -o .bin/swagger github.com/go-swagger/go-swagger/cmd/swagger
RUN go build -o .bin/goimports golang.org/x/tools/cmd/goimports
RUN go build -o .bin/swagutil github.com/ory/sdk/swagutil
RUN go build -o .bin/packr2 github.com/gobuffalo/packr/v2/packr2

RUN .bin/packr2
RUN GO111MODULE=on GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build ${LDFLAGS} -o kratos .
#RUN GO111MODULE=on go install .
RUN .bin/packr2 clean


FROM alpine

LABEL maintainer="admin@3pi.dev"
RUN mkdir -p /etc/kratos/
COPY --from=builder /go/src/github.com/ory/kratos/kratos  /usr/bin/kratos
#COPY --from=builder $(go env GOPATH)/bin/kratos /usr/bin/kratos

RUN addgroup -S kratos && adduser -S -G kratos kratos

USER kratos

ENTRYPOINT [ "/usr/bin/kratos" ]

VOLUME [ "/etc/kratos" ]

CMD [ "run" ]
# CMD [ "run", "-c", "/etc/kratos/kratos.yaml"]

#EXPOSE 8000 8090
